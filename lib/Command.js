import Bluebird from 'bluebird';
import fs from 'fs';

export default class Command {
  readFromFile(fileName) {
    return new Bluebird(function(resolve, reject) {
      if (!fileName) {
        reject(new Error('File name is required!'));
      }

      fs.readFile(fileName, function(err, data) {
        if (err) {
          reject(err);
        }

        resolve(data);
      });
    })
  }

  formatPlaceCmd(cmd) {
    const placeCmd = cmd && cmd.split(" ");
    if (!placeCmd || placeCmd[0] !== 'PLACE' || placeCmd.length !== 2) {
      throw new Error(`Invalid PLACE comamnd (${cmd}). Must be in the form (PLACE x,y,face)!`);
    }

    return {
      name: placeCmd[0],
      params: placeCmd[1].split(",")
    }
  }

  formatData(data) {    
    if (!data) {
      return [];
    }

    let cmds = data.toString().split("\n");

    //return array of valid commands after ignoring empty lines
    return cmds.reduce((arr, cmd) => {
      cmd = cmd.trim().toUpperCase();

      // Ignore empty lines
      if (!cmd || cmd.length === 0) {
        return arr;
      }

      //for 'PLACE'' commands convert them into object { name, params }
      cmd.substring(0, 5) === 'PLACE' ? arr.push(this.formatPlaceCmd(cmd)) : arr.push(cmd);  

      return arr;
    }, [])
  }
}