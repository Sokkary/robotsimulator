import Utils from './Utils';

export default class Robot {
  static DIRECTIONS = {
    EAST: 'EAST',
    WEST: 'WEST',
    NORTH: 'NORTH',
    SOUTH: 'SOUTH'
  }

  static COMMANDS = {
    PLACE: 'PLACE',
    MOVE: 'MOVE',
    LEFT: 'LEFT',
    RIGHT: 'RIGHT',
    REPORT: 'REPORT'
  }

  static ROTATIONS = {
    LEFT: 'LEFT',
    RIGHT: 'RIGHT'
  }

  constructor() {
    this.isOnBoard = false;
  }

  place(x, y, faceDir = this.faceDir) {
    if (x == null || x < 0) {
      throw new Error(`Invalid robot X position (${x})`);
    }

    if (y == null || y < 0) {
      throw new Error(`Invalid robot Y position (${y})`);
    }

    if (!faceDir || !Robot.DIRECTIONS[faceDir]) {
      throw new Error(`Invalid robot face direction (${faceDir})`);
    }

    this.x = x;
    this.y = y;
    this.faceDir = faceDir;

    this.isOnBoard = true;
  }

  virtualMove() {
    let { x, y } = this;

    if (!this.isOnBoard) {
      return { x, y };
    }

    const ROBOT_MOVES = {
      [Robot.DIRECTIONS.EAST]: () => ({ x: ++x, y }),
      [Robot.DIRECTIONS.WEST]: () => ({ x: --x, y }),
      [Robot.DIRECTIONS.NORTH]: () => ({ x, y: ++y }),
      [Robot.DIRECTIONS.SOUTH]: () => ({ x, y: --y }),
    };

    return ROBOT_MOVES[this.faceDir]();
  }

  rotate(rotation) {
    if (!this.isOnBoard) {
      return;
    }

    if (rotation !== Robot.ROTATIONS.LEFT && rotation !== Robot.ROTATIONS.RIGHT) {
      throw new Error(`Invalid rotation (${rotation})!`);      
    }

    const ROBOT_ROTATIONS = {
      [Robot.DIRECTIONS.EAST]: robotRotation => robotRotation === Robot.ROTATIONS.LEFT ? Robot.DIRECTIONS.NORTH : Robot.DIRECTIONS.SOUTH,
      [Robot.DIRECTIONS.NORTH]: robotRotation => robotRotation === Robot.ROTATIONS.LEFT ? Robot.DIRECTIONS.WEST : Robot.DIRECTIONS.EAST,
      [Robot.DIRECTIONS.WEST]: robotRotation => robotRotation === Robot.ROTATIONS.LEFT ? Robot.DIRECTIONS.SOUTH : Robot.DIRECTIONS.NORTH,
      [Robot.DIRECTIONS.SOUTH]: robotRotation => robotRotation === Robot.ROTATIONS.LEFT ? Robot.DIRECTIONS.EAST : Robot.DIRECTIONS.WEST,
    };

    return this.faceDir = ROBOT_ROTATIONS[this.faceDir](rotation);
  }

  left() {
    return this.rotate(Robot.ROTATIONS.LEFT);
  }

  right() {
    return this.rotate(Robot.ROTATIONS.RIGHT);
  }

  report() {
    if (!this.isOnBoard) {
      return;
    }

    Utils.log(`${this.x}, ${this.y}, ${this.faceDir}`);
  }

  executeCommand(cmd, isValidLocationFn) {
    if (cmd.name === Robot.COMMANDS.PLACE) {
      return this.place(...cmd.params);
    }

    if (!this.isOnBoard) {
      return;
    }

    if (cmd === Robot.COMMANDS.MOVE) {
      let newLocation = this.virtualMove();
      return isValidLocationFn(newLocation) && this.place(newLocation.x, newLocation.y);
    }

    if (cmd === Robot.COMMANDS.LEFT || cmd === Robot.COMMANDS.RIGHT || cmd === Robot.COMMANDS.REPORT) {
      return this[cmd.toLowerCase()]();
    }

    Utils.log(`Invalid command (${cmd})`);
    
    return false;
  }
}