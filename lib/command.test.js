import chai from 'chai';
import sinon from 'sinon';

import Command from './command';

import fs from 'fs';

const expect = chai.expect;

describe('Command Class', () => {
  let command;

  beforeEach(() => command = new Command());

  describe('readFromFile()', () => {
    let sandbox;

    before(() => {
      sandbox = sinon.sandbox.create();
      sinon.stub(fs, 'readFile', (fileName, callback) => callback(null, 'TEST DATA'))
    })

    after(() => sandbox.restore());

    it('should return a promise', () => {
      const p = command.readFromFile('test');
      expect(p.then).to.be.a('function');
    });

    it('should return error if no file name params passed in', (done) => {
      command.readFromFile()
        .then(() => done(new Error('Expected error but got valid results')))
        .catch(err => {
          expect(err).to.be.an('error');
          done();
        })
    });
  });

  describe('formatPlaceCmd()', () => {
    it('should throw error if no command passed in', () => {
      expect(() => command.formatPlaceCmd()).to.throw(Error);
    });

    it('should throw error if splitted command parts not equal 2', () => {
      expect(() => command.formatPlaceCmd('PLACE')).to.throw(Error);
    });

    it('should throw error if splitted command[0] not equal PLACE', () => {
      expect(() => command.formatPlaceCmd('place x,y,f')).to.throw(Error);
    });

    it('should return object of { name, params } for valid PLACE cmd', () => {
      const cmd = command.formatPlaceCmd('PLACE x,y,f');
      expect(cmd).to.be.an('object');
      expect(cmd.name).to.equal('PLACE');
      expect(cmd.params).to.be.an('array');
    });

    it('should return params as array of [x, y, f]', () => {
      const cmd = command.formatPlaceCmd('PLACE x,y,f');
      expect(cmd.params).to.be.an('array');
      expect(cmd.params[0]).to.equal('x');
      expect(cmd.params[1]).to.equal('y');
      expect(cmd.params[2]).to.equal('f');
    });
  });

  describe('formatData()', () => {
    let sandbox;

    beforeEach(function() {
      sandbox = sinon.sandbox.create();      
    });

    afterEach(function() {
      sandbox.restore();
    });

    it('Should return empty array if no data passed', () => {
      const cmds = command.formatData();
      expect(cmds).to.be.an('array');
      expect(cmds.length).to.equal(0);
    });

    it('Should return array of 3 cmds', () => {
      const data = `
        PLACE 0,0,NORTH
        MOVE
        REPORT
      `;
      const cmds = command.formatData(data);
      expect(cmds).to.be.an('array');
      expect(cmds.length).to.equal(3);
    });

    it('Should ignore spaces and empty lines', () => {
      const data = `
        PLACE 0,0,NORTH

        MOVE

          REPORT
      `;
      const cmds = command.formatData(data);
      expect(cmds).to.be.an('array');
      expect(cmds.length).to.equal(3);
    });

    it('Should accept cmds in small letters and return them in capital letters', () => {
      const data = `move`;
      const cmds = command.formatData(data);
      expect(cmds[0]).to.equal('MOVE');
    });

    it('Should call .formatPlaceCmd() for PLACE cmds', () => {
      const data = `
        PLACE x,y,f
        MOVE
        PLACE x,y,f
      `;
      
      sandbox.stub(command, "formatPlaceCmd");
      command.formatData(data);
      sinon.assert.calledTwice(command.formatPlaceCmd);
    });
  });
});