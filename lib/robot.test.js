import chai from 'chai';
import sinon from 'sinon';

import Robot from './robot';
import Utils from './Utils';

import fs from 'fs';

const expect = chai.expect;

describe.only('Robot Class', () => {
  let robot;

  beforeEach(() => robot = new Robot());

  describe('constructor()', () => {
    it('should init isOnboard to false', () => {
      expect(robot.isOnBoard).to.equal(false);
      expect(robot.x).to.be.undefined;
      expect(robot.y).to.be.undefined;
    });

    it('should x, y and faceDir to be undefined', () => {
      expect(robot.x).to.be.undefined;
      expect(robot.y).to.be.undefined;
      expect(robot.faceDir).to.be.undefined;
    });
  });

  describe('place()', () => {
    it('should throw error on X position not passed in', () => {
      expect(() => robot.place(null, 1, 'EAST')).to.throw(Error);
    });

    it('should throw error if X < 0', () => {
      expect(() => robot.place(-1, 1, 'EAST')).to.throw(Error);
    });

    it('should throw error on Y position not passed in', () => {
      expect(() => robot.place(1, null, 'EAST')).to.throw(Error);
    });

    it('should throw error if Y < 0', () => {
      expect(() => robot.place(1, -1, 'EAST')).to.throw(Error);
    });

    it('should throw error if faceDir not passed in', () => {
      expect(() => robot.place(1, 1, null)).to.throw(Error);
    });

    it('should throw error if faceDir is not one of EAST, WEST, NORTH or SOUTH', () => {
      expect(() => robot.place(1, 1, 'XYZ')).to.throw(Error);
    });

    it('should not throw error if faceDir is not one of EAST, WEST, NORTH or SOUTH', () => {
      expect(() => robot.place(1, 1, 'EAST')).not.to.throw(Error);
      expect(() => robot.place(1, 1, 'EAST')).not.to.throw(Error);
      expect(() => robot.place(1, 1, 'EAST')).not.to.throw(Error);
      expect(() => robot.place(1, 1, 'EAST')).not.to.throw(Error);
    });

    it('should update x,y,faceDir', () => {
      robot.place(1, 2, 'SOUTH');
      expect(robot.x).to.equal(1);
      expect(robot.y).to.equal(2);
      expect(robot.faceDir).to.equal('SOUTH');
    });

    it('should set isOnBoard to true', () => {
      robot.place(1, 2, 'SOUTH');
      expect(robot.isOnBoard).to.equal(true);
    });
  });

  describe('virtualMove()', () => {
    it('should ignore the move if isOnBoard = false', () => {
      robot.place(1, 2, 'EAST');
      robot.isOnBoard = false;
      const location = robot.virtualMove();
      expect(location.x).to.equal(1);
      expect(location.y).to.equal(2);
    });

    it('should not change the robot X,Y', () => {
      robot.place(1, 1, 'NORTH');
      const location = robot.virtualMove();
      expect(robot.x).to.equal(1);
      expect(robot.y).to.equal(1);
    });

    it('should increment X when faceDir to EAST', () => {
      robot.place(1, 1, 'EAST');
      const location = robot.virtualMove();
      expect(location.x).to.equal(2);
    });

    it('should decrement X when faceDir to WEST', () => {
      robot.place(1, 1, 'WEST');
      const location = robot.virtualMove();
      expect(location.x).to.equal(0);
    });

    it('should increment Y when faceDir to NORTH', () => {
      robot.place(1, 1, 'NORTH');
      const location = robot.virtualMove();
      expect(location.y).to.equal(2);
    });

    it('should decrement Y when faceDir to SOUTH', () => {
      robot.place(1, 1, 'SOUTH');
      const location = robot.virtualMove();
      expect(location.y).to.equal(0);
    });
  });

  describe('rotate()', () => {
    it('should throw error if the rotation direction is not LEFT or RIGHT', () => {
      robot.place(1, 1, 'EAST');
      expect(() => robot.rotate('XYZ')).to.throw(Error);
    });

    it('should ignore the move if isOnBoard = false', () => {
      robot.place(1, 2, 'EAST');
      robot.isOnBoard = false;
      robot.rotate('LEFT');
      expect(robot.faceDir).to.equal('EAST');
    });

    it('should change direction to NORTH when LEFT from EAST', () => {
      robot.place(1, 1, 'EAST');
      robot.rotate('LEFT');
      expect(robot.faceDir).to.equal('NORTH');
    });

    it('should change direction to WEST when LEFT from NORTH', () => {
      robot.place(1, 1, 'NORTH');
      robot.rotate('LEFT');
      expect(robot.faceDir).to.equal('WEST');
    });

    it('should change direction to SOUTH when LEFT from WEST', () => {
      robot.place(1, 1, 'WEST');
      robot.rotate('LEFT');
      expect(robot.faceDir).to.equal('SOUTH');
    });

    it('should change direction to EAST when LEFT from SOUTH', () => {
      robot.place(1, 1, 'SOUTH');
      robot.rotate('LEFT');
      expect(robot.faceDir).to.equal('EAST');
    });

    it('should change direction to SOUTH when RIGHT from EAST', () => {
      robot.place(1, 1, 'EAST');
      robot.rotate('RIGHT');
      expect(robot.faceDir).to.equal('SOUTH');
    });

    it('should change direction to WEST when RIGHT from SOUTH', () => {
      robot.place(1, 1, 'SOUTH');
      robot.rotate('RIGHT');
      expect(robot.faceDir).to.equal('WEST');
    });

    it('should change direction to NORTH when RIGHT from WEST', () => {
      robot.place(1, 1, 'WEST');
      robot.rotate('RIGHT');
      expect(robot.faceDir).to.equal('NORTH');
    });

    it('should change direction to EAST when RIGHT from NORTH', () => {
      robot.place(1, 1, 'NORTH');
      robot.rotate('RIGHT');
      expect(robot.faceDir).to.equal('EAST');
    });
  });

  describe('left()', () => {
    let sandbox;

    before(() => sandbox = sinon.sandbox.create())
    after(() => sandbox.restore());

    it('should call rotate with LEFT rotation', () => {
      sandbox.stub(robot, "rotate");
      robot.left();
      expect(robot.rotate.calledOnce).to.equal(true);
      expect(robot.rotate.calledWith('LEFT')).to.equal(true);
    });
  });

  describe('right()', () => {
    let sandbox;

    before(() => sandbox = sinon.sandbox.create())
    after(() => sandbox.restore());

    it('should call rotate with RIGHT rotation', () => {
      sandbox.stub(robot, "rotate");
      robot.right();
      expect(robot.rotate.calledOnce).to.equal(true);
      expect(robot.rotate.calledWith('RIGHT')).to.equal(true);
    });
  });

  describe('executeCommand()', () => {
    let sandbox, board;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();

      board = {
        isValidLocation: location => true
      }
      sandbox.stub(robot, 'place');
      sandbox.stub(robot, 'virtualMove', () => ({ x: 0, y: 0 }));
      sandbox.stub(robot, 'left');
      sandbox.stub(robot, 'right');
      sandbox.stub(robot, 'report');
    })

    afterEach(() => sandbox.restore());

    it('should ignore to command if isOnBoard = false and command is not PLACE', () => {
      robot.isOnBoard = false;
      robot.executeCommand('MOVE', board.isValidLocation);
      expect(robot.virtualMove.calledOnce).to.equal(false);
    });

    it('should throw error if the cmd not one of PLACE,MOVE,LEFT,RIGHT or REPORT', () => {
      robot.isOnBoard = true;
      expect(robot.executeCommand('XYZ')).to.equal(false);
    });

    it('should call .virtualMove() when passing MOVE command', () => {
      robot.isOnBoard = true;
      robot.executeCommand('MOVE', board.isValidLocation);
      expect(robot.virtualMove.calledOnce).to.equal(true);
    });

    it('should call .place() when passing MOVE command', () => {
      robot.isOnBoard = true;
      robot.executeCommand('MOVE', board.isValidLocation);
      expect(robot.place.calledOnce).to.equal(true);
    });

    it('should call isValidLocation() when passing MOVE command', () => {
      robot.isOnBoard = true;
      sandbox.stub(board, 'isValidLocation');
      robot.executeCommand('MOVE', board.isValidLocation);
      expect(board.isValidLocation.calledOnce).to.equal(true);
    });

    it('should call .left() when passing LEFT command', () => {
      robot.isOnBoard = true;
      robot.executeCommand('LEFT', board.isValidLocation);
      expect(robot.left.calledOnce).to.equal(true);
    });

    it('should call .right() when passing RIGHT command', () => {
      robot.isOnBoard = true;
      robot.executeCommand('RIGHT', board.isValidLocation);
      expect(robot.right.calledOnce).to.equal(true);
    });

    it('should call .report() when passing REPORT command', () => {
      robot.isOnBoard = true;
      robot.executeCommand('REPORT', board.isValidLocation);
      expect(robot.report.calledOnce).to.equal(true);
    });

  });
});