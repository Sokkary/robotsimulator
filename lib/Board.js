import Utils from './Utils';

export default class Board {
  constructor(width = 0, height = 0) {
    this.width = width;
    this.height = height;
  }

  resize(width = this.width, height = this.height){
    this.width = width;
    this.height = height;
  }

  isValidLocation(location) {
    if (location.x == null || location.x < 0 || location.x >= this.width) {
      Utils.log(`Invalid robot X position (${location.x}). X position must be between 0 - ${this.width - 1}`);
      return false;
    }

    if (location.y == null || location.y < 0 || location.y >= this.height) {
      Utils.log(`Invalid robot Y position (${location.y}). Y position must be between 0 - ${this.height - 1}`);
      return false;
    }

    return true;
  }
}