import chai from 'chai';

import Board from './board';

const expect = chai.expect;

describe('Board Class', () => {
  let board;

  beforeEach(() => board = new Board(4, 6));

  describe('constructor()', () => {
    it('should initialize board with passed width & height', () => {
      expect(board.width).to.equal(4);
      expect(board.height).to.equal(6);
    });

    it('should initialize board defaults if no width or height given', () => {
      const defaultBoard = new Board()
      expect(defaultBoard.width).to.equal(0);
      expect(defaultBoard.height).to.equal(0);
    });
  });

  describe('isValidLocation()', () => {
    it('should return false if X > width', () => {
      const result = board.isValidLocation({ x: 10, y: 0 });
      expect(result).to.equal(false);
    });

    it('should return false if X < 0', () => {
      const result = board.isValidLocation({ x: 5, y: 0 });
      expect(result).to.equal(false);
    });

    it('should return false if Y > height', () => {
      const result = board.isValidLocation({ x: 0, y: 10 });
      expect(result).to.equal(false);
    });

    it('should return false if Y < 0', () => {
      const result = board.isValidLocation({ x: 0, y: 10 });
      expect(result).to.equal(false);
    });

    it('should return true if x & y within the board width & height', () => {
      const result = board.isValidLocation({ x: 1, y: 1 });
      expect(result).to.equal(true);
    });
  });  
});