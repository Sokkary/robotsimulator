# REA Robot Simulator

This is NodeJs console/text based simulates Robot movements. The instructions will be loaded from a text file "input.txt" 

## Instructions
The Robot commands should be in the following format where each command on a separate line. We can add multiple sets of commands and the robot will ignore all invalid commands or empty lines. 
- Example for valid instructions:

        PLACE 1,2,EAST
        MOVE
        LEFT
        MOVE
        RIGHT
        REPORT
 
 
We have 3 main classes.

- **Board:** For the board layout and location validation.

- **Robot:** Can understand and execute simple commands [PLACE, MOVE, LEFT, RIGHT, REPORT].

- **Command:** Responsible for loading the data from 'input.txt' file and preparing list of formatted commands. 

  
## Run
    npm i && npm start

## Test
    npm test
  
## Notes
For time consideration, I focused on the main job of Robot moves and implemented basic instructions loader from text file. Each task can be exntended like
- Loading data from file through console args
- Validating & restricting file size
- Streaming data directly from file to the Robot
- ...

