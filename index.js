import { Command, Robot, Board, Utils } from './lib';

function main() {
  const command = new Command();
  const board = new Board(5, 5);
  const robot = new Robot();

  command.readFromFile('input.txt')  
    .then(data => command.formatData(data))
    .each(cmd => robot.executeCommand(cmd, board.isValidLocation.bind(board)))
    .catch(err => console.error('Error: ', err));
}

//init & run
main();

//catchup clean exit, CTRL + C or unhandled Exceptions
Utils.cleanup(() => Utils.log('\n *** App Exit & cleanup ***'));
